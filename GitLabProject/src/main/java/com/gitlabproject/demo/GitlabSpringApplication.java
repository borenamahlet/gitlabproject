package com.gitlabproject.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabSpringApplication.class, args);
    }

}
